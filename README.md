# KJD TUTORIALS
Video training and knowledge sharing.

### Prerequisites
- Docker
- Docker-Compose

## Container Software
- CentOS
- PHP
- MONGODB

## How to control the container ?
- Navigate to the root of the cloned folder.
#### Start
- docker-compose up -d
#### Stop
- docker-compose stop
#### Remove
- docker-compose rm

## Networking
--------------------
### Localhost
ports:
  - "8080:80"
  - "8443:443"
### Bridge
- network_mode: bridge

# Check your containers status
- docker ps
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                         NAMES
5bc3b6f4ea72        kjdtutorials_app    "/run-httpd.sh"          5 minutes ago       Up 5 minutes        0.0.0.0:8080->80/tcp, 0.0.0.0:8443->443/tcp   kjdtutorials_app_1
a35c2666de21        mongo               "docker-entrypoint..."   5 minutes ago       Up 5 minutes        27017/tcp                                     kjdtutorials_db_1
```
