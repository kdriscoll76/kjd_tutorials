<?php
date_default_timezone_set("America/New_York");
#// Reformat MONGO ID //#
function fix_id($data){
 $filter[0] = '({"_id":)';
 $filter[1] = '("},")';
 $filter[2] = '"\$id"';
 $replace[0] = '';
 $replace[1] = '","';
 $replace[2] = 'id';
 return preg_replace($filter,$replace,$data);
}
#// Account Info //#
function account_query(){
 $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
 $col = $con->accounts->users;
 $data = $col->findOne();
 return $data;
}
function all_account_query(){
 $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
 $col = $con->accounts->users;
 $data = $col->find();
 $data = json_encode(iterator_to_array($data,false),true);
 return fix_id($data);
}

function create_account($fields){
  $fields['date'] = date('r');
  $username = $fields['username'];
  $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
  $col = $con->accounts->users;
  $col->update(array("username" => $username ),array('$set' => $fields ),array( "upsert" => true, "multi" => true));
}

function account_delete($id){
  $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
  $col = $con->accounts->users;
  $col->remove(array("_id" => new MongoId($id)));
}
#// API Functions //#
function upload_file($fields){
 $fields['date'] = date('r');
 $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
 $col = $con->tutorial->videos;
 $col->insert($fields);
}
#// Query Records //#
function query_all_vids(){
 $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
 $col = $con->tutorial->videos;
 $data = $col->find();
 $data = json_encode(iterator_to_array($data,false),true);
 return fix_id($data);
}
function query_vids($id){
 $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
 $col = $con->tutorial->videos;
 $data = $col->findOne(array("_id" => new MongoId($id)));
 return fix_id($data);
}
function remove_rec($id){
  $con = new Mongo("mongodb://kjdtutorials_db_1.kjdtutorials_default:27017");
  $col = $con->tutorial->videos;
  $col->remove( array( '_id' => new MongoId($id) ) );
}
function tmp_pass(){
 $abc = range(a,z);
 $num = '0123456789';
 $characters = $abc;
 $characters .= $num;
 $string = '';
 $max = strlen($characters) + 1;
 for($i = 1;$i < 11;$i++){
  $string .= $characters[mt_rand(0, $max)];
 }
 return $string;
}
 ?>
