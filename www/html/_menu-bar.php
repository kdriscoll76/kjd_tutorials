<nav class='nav navbar navbar-inverse navbar-fixed-top'>
	<?php
	$config = json_decode( file_get_contents('../data/_config.json'));
	 if(isset($_SESSION['user'])){
	echo"
	<div class='nav navbar-text pull-right'>
    <label>User: ".$_SESSION['user']."</label>
    <br/>
    <label>Role: ".$_SESSION['role']."</label>
  </div>
	";
  }
	?>
  <div class='container-fluid'>
  <div class='navbar-header'>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
  <div class='navbar-brand'>
    <?php echo $config->sitename;?>
		- <?php echo $page;?>
  </div>
<div class='collapse navbar-collapse' id="myNavbar">
  <ul class="nav navbar-nav">
  <li><a href='index.php'>HOME</a></li>
	<?php
	if(isset($_SESSION['user'])){
   echo"<li><a href='create_vid.php'>CREATE</a></li>";
  }
	?>
  <li><a href='dashboard_vid.php'>DASHBOARD</a></li>
  <li><a href='rss_vid.php'>RSS</a></li>
  <li><a href='api/'>API</a></li>
	<?php
	if(isset($_SESSION['user'])){
	 if( $_SESSION['role'] == 'admin'){
		 echo "<li><a href='manage.php'>MANAGE</a></li>";
	 }
		echo"<li><a href='logout.php'>LOGOUT</a></li>";
   }else{
    echo "<li><a href='logon.php'>LOGIN</a></li>";
   }
	 ?>
  </ul>
</div>
</nav>
