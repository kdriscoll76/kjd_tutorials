<?php
require_once("_header");
require_once("_functions.php");
$vids = json_decode( query_all_vids(),true);
?>
<div class='table-responsive'>
<table id='table1' class='table table-border'>
<thead>
<tr>
  <th>TITLE</th>
  <th>DETAILS</th>
</tr>
</thead>
<tbody>
  <?php
  foreach($vids as $obj){
    $id = $obj['id'];
    $vid_name = strtoupper(preg_replace("/\..*$/",'',$obj['vid']));
   echo "<tr>";
    if(!empty($obj['thumbnail'])){
      echo"<td><img width='300px' src='vids/".$obj['thumbnail']."'></td>";
    }else{
      echo "
    <td class='vid_title text-center'>
     <h2>".strtoupper( $obj['vid-title'])."</h2>
    </td>";
    }
    echo"
     <td>
      <h2>$vid_name</h2>
      <b>Upload Date:</b>
      <div>".$obj['date']."</div>
      <b>Summary:</b>
      <div>".$obj['summary']."</div>
      <label>Type: ".$obj['type']."</label>
      <br/>
      <label>Author: ".$obj['author']."</label>
      <a class='btn btn-primary pull-right space' href='view_vid.php?id=".$obj['id']."'>Watch Now</a>
      <a class='btn btn-danger pull-right space' href='remove_rec.php?id=".$obj['id']."'>Delete</a>
     </td>
    </tr>";
  }
  ?>
</tbody>
</table>
</div>
<script type='text/javascript'>
 $("#table1").DataTable();
</script>
