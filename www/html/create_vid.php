<?php
session_start();
$page = 'Create';
require_once("_header");
require_once("_functions.php");
require_once("_menu-bar.php");
 ?>
 <div style='padding-top:150px;' class='container'>
   <div class='row'>
     <div class='panel panel-info'>
       <div class='panel-heading'>
         New tutorial
       </div>
       <form action='process_vid.php' method='post' enctype='multipart/form-data'>
         <input type='hidden' name='author' value="<?php echo $_SESSION['user']; ?>"/>
       <div class='panel-body'>
         <input type='text' class='form-control' name='vid-title' placeholder='Title'/>
          <label>thumbnail:
            <i>Max file size 1gb</i>
           <input type='file' class='well' name='vid_thumnail' id="thumnail"/>
          </label>
          <label>Video:
            <i>Max file size 1gb</i>
           <input type='file' class='well' name='vid_data' id="file"/>
          </label>
         <input type='text' class='form-control' name='vid_tag' placeholder='Add keyword tags seperated by comma.' />
         <textarea name='summary' class='form-control'>Add brief summary of the video here.</textarea>
       </div>
       <div class='panel-footer'>
         <button type='submit' class='btn btn-success'>Upload</button>
         <a class='btn btn-danger' href='index.php'>Cancel</a>
       </div>
     </form>
   </div>
 </div>
