<?php
require("_header");
require("_functions.php");
$vids = json_decode( query_all_vids(),true);
foreach( $vids as $vid ){
  $vid_name = strtoupper(preg_replace("/\..*$/",'',$vid['vid']));
 $data[] = array(
   "title" => $vid_name,
   "link"  => $vid['link'],
   "thumbnail" => $vid['thumbnail']
 );
}
$data[] = array(
  "title" => "home",
  "link" => "index.php"
);
 ?>
 <body style='background-color:#000000;'>
   <h1 class='text-center text-primary'>New Releases</h1>
   <div class='container'>
    <div class='row space'>
      <?php
       foreach( $data as $obj ){
        echo "<button onclick=\"window.location.href='".$obj['link']."'\"
        class='btn btn-primary widget col-xs-12 col-sm-4 space'>";
        if(!empty($obj['thumbnail'])){
         echo "<span><img width='200px' src='vids/".$obj['thumbnail']."'><pre>".$obj['title']."</pre></span>";
        }else{
         echo "<span class='wordwrap'>".strtoupper($obj['title'])."</span>";
        }
        echo"
        </button>";
       }
       ?>
    </div>
   </div>
