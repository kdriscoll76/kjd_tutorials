<?php
session_start();
if( $_SESSION['role'] != 'admin' ){
  header("location:index.php");
}
require('_header');
$page = 'Manage';
require('_menu-bar.php');
require('_functions.php');
$tmpass = tmp_pass();
$accounts = json_decode( all_account_query(),true);
if(!empty($_POST['username'])){
  create_account($_POST);
}
 ?>
 <div style='padding-top:150px;' class='container'>
   <div class='row'>
     <div class='panel panel-info'>
       <div class='panel-heading'>
         <div class='panel-title'>ACCOUNT MANAGEMENT</div>
         <button id='new_acc_btn' class='space btn btn-success'>NEW ACCOUNT</button>
         <div id='new_acc_form' class='well shadow'>
           <h4>NEW ACCOUNT</h4>
         <form method='post'>
           <div class='form-group'>
             <label class=''>USERNAME:
             <input class='form-control' type='text' name='username' placeholder="Username" />
           </label>
             <label>TEMP PASS:
             <input class='form-control' type='text' name='password' value="<?php echo $tmpass; ?>" />
           </label>
         </div>
         <div>
             <label>Email:
             <input class='form-control' type='email' name='email' placeholder="Email" />
           </label>
         </div>
             <label>ROLE:
             <select class='form-control' name='role'>
               <option>user</option>
               <option>admin</option>
             </select>
           </label>
           <button type='submit' class='btn btn-warning'>CREATE</button>
         </form>
       </div>
     </div>
       <div class='panel-body'>
         <table id='table1' class='table'>
           <thead>
             <tr>
               <?php
                if($_SESSION['role'] == 'admin'){
                  echo "<th>#</th>";
                }
               ?>
               <th>USER</th>
               <th>ROLE</th>
               <th>EMAIL</th>
               <th>DATE</th>
             </tr>
           </thead>
           <tbody>
             <?php
             foreach($accounts as $obj){
               $id = $obj['id'];
             echo"<tr>";
                 if($_SESSION['role'] == 'admin'){
                   echo "<td><a href='delete_account.php?id=$id'>delete</a></td>";
                 }
                 echo"
                 <td>".$obj['username']."</td>
                 <td>".$obj['role']."</td>
                 <td>".$obj['email']."</td>
                 <td>".$obj['date']."</td>
                 </tr>";
             }
             ?>
           </tbody>
           <tr>
         </table>
       </div>
     </div>
   </div>
   <div class='row'>
   </div>
 </div>
 <script type='text/javascript'>
  $("#new_acc_form").hide();
  $("#table1").DataTable();
  $("#new_acc_btn").click(function(){
    $("#new_acc_form").toggle();
  });
 </script>
