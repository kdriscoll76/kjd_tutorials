<?php
 session_start();
 require("_header");
 require("_functions.php");
 $config = json_decode( file_get_contents('../data/_config.json'));
 $storage = $config->storage;
 $allowedExts = array("jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma");
 $extension = pathinfo($_FILES['vid_data']['name'], PATHINFO_EXTENSION);
  if ($_FILES["vid_data"]["error"] > 0){
    echo "<div class='alert alert-danger' role='alert'>
    <strong>ERROR</strong> " . $_FILES['vid_data']['error'] . "
    </div>";
    }else{
     $upload = $_FILES["vid_data"]["name"];
     $type = $_FILES["vid_data"]["type"];
     $size = ( $_FILES["vid_data"]["size"] / 1024 );
     if (file_exists($storage . $_FILES["vid_data"]["name"])){
       echo "<div class='alert alert-warning' role='alert'>
       <strong>WARNING</strong> " . $_FILES['vid_data']['name'] . "
       Already exists!</div>";
       }else{
       move_uploaded_file($_FILES["vid_data"]["tmp_name"],$storage . $_FILES["vid_data"]["name"]);
       move_uploaded_file($_FILES["vid_thumnail"]["tmp_name"],$storage . $_FILES["vid_thumnail"]["name"]);
       $_POST['thumbnail'] = $_FILES["vid_thumnail"]["name"];
       $_POST['vid'] = $_FILES['vid_data']['name'];
       $_POST['size'] = $size;
       $_POST['type'] = $type;
       $_POST['user'] = $user;
       upload_file($_POST);
       echo"<div class='alert alert-success' role='alert'>
       <strong>Uplosded</strong>".$_FILES['vid_data']['name']."
       </div>";
      }
    }
   header("location:index.php");
?>
