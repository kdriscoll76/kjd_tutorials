<?php
require("_header");
 ?>
 <div class='container'>
   <div class='space'>
   <div class='panel panel-info col-xs-12 col-sm-6 shadow'>
     <div class='panel-heading'>
       <div class='panel-title'>Register Account</div>
     </div>
     <div class='panel-body'>
       <form method=post>
        <input class='form-control' type='text' name='username' placeholder="Username" maxlength="20"/>
        <input class='form-control' type='email' name='email' placeholder="Email Address" maxlength="50"/>
        <div class='space'>
         <button type='submit' class='btn btn-primary'>Request</button>
         <a class='btn btn-danger' href='/'>Cancel</a>
        </div>
       </form>
     </div>
   </div>
 </div>
 </div>
