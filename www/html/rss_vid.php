<?php
require("_functions.php");
date_default_timezone_set('America/New_York');
header("Content-Type: application/xml; charset=ISO-8859-1");
$data = json_decode( query_all_vids(),true);
?>
 <rss version='2.0'>
     <channel>
       <title>Videos</title>
       <description></description>
       <link>http://127.0.0.1:8080</link>
       <language>en-us</language>
       <copyright>kjdtutorials.com</copyright>
       <?php
       foreach( $data as $obj ){
         echo"
          <item>
            <title>".$obj['vid-title']."</title>
            <description>".$obj['summary']."</description>
            <link>http://127.0.0.1:8080/view_vid.php?id=".$obj['id']."</link>
            <pubDate>".$obj['date']."</pubDate>
          </item>";
        }
          ?>
     </channel>
   </rss>
