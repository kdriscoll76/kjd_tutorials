<?php
session_start();
require("_header");
require("_menu-bar.php");
require("_functions.php");
 $obj = query_vids($_GET['id']);
 ?>
<div style='padding-top:110px;' class='container'>
  <div class='row'>
    <div>
    <h2><?php echo $obj['vid-title'];?></h2>
    </div>
    <div class='col-xs-12'>
      <video class='shadow col-xs-12' width="800" height="400" controls>
       <source src="vids/<?php echo $obj['vid'];?>" type="<?php echo $obj['type'] ;?>">
       Your browser does not support the video tag.
      </video>
    </div>
    <br/>
    <div class='col-xs-12 well space shadow'>
     <b>Details</b>
     <p><?php echo $obj['summary'];?></p>
    </div>
    <div class='col-xs-12 well space'>
     <b>Comments</b>
     <?php
       if(!isset($_SESSION['user'])){
         echo "<i class='text-danger'>Must be logged in to comment.</i>";
       }else{
         echo"
      <form method='post'>
        <div class='space'>
         <input type='hidden' name='author' value='<?php echo $username; ?>'\>
         <textarea class='form-control' name='comment'></textarea>
        </div>
        <button class='btn btn-success' type='submit'>Add Comment</button>
      </form>";
       }
       ?>
      <div class='space'>
      <?php include("comments.php");?>
      </div>
    </div>
  </div>
</div>
